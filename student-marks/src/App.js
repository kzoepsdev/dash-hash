//Step 2: change the base code to use Component
//Step 2: change the base code to use Component
import React, { Component } from 'react'
import { STUDENT_LIST_ABI, STUDENT_LIST_CONTRACT } from './abi/config_studentList';
import Web3 from 'web3'
import './App.css';
//We replace function with a component for better management of code
class App extends Component {
  //componentDidMount called once on the client after the initial render
  //when the client receives data from the server and before the data is displayed.
  componentDidMount() {
    if (!window.ethereum)
      throw new Error("No crypto wallet found. Please install it.");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
  }
  //create a web3 connection to using a provider, MetaMask to connect to
  //the ganache server and retrieve the first account
  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "http://localhost:7545")
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })
    //Step 4: load all the lists from the blockchain
    const studentList = new web3.eth.Contract(
      STUDENT_LIST_ABI, STUDENT_LIST_CONTRACT)
    //Keep the lists in the current state
    this.setState({ studentList })
    //Get the number of records for all list in the blockchain
    const studentCount = await studentList.methods.studentsCount().call()
    //Store this value in the current state as well
    this.setState({ studentCount })
    //Use an iteration to extract each record info and store
    //them in an array
    this.state.students = []
    for (var i = 1; i <= studentCount; i++) {
      const student = await studentList.methods.students(i).call()
      this.setState({
        students: [...this.state.students, student]
      })
    }

  }
  //Initialise the variables stored in the state
  constructor(props) {
    super(props)
    this.state = {
      account: '',
      studentCount: 0,
      students: []
    }
  }
  //Display the first account
  render() {
    return (
      <div className="container">
        <h1>Hello, World!</h1>
        <p>Your account: {this.state.account}</p>
        <ul id="studentList" className="list-unstyled">
          {
            //This gets the each student from the studentList
            //and pass them into a function that display the
            //details of the student
            this.state.students.map((student, key) => {
              return (
                <li className="list-group-item checkbox" key={key}>
                  <span className="name alert">{student._id}. {student.cid} {student.name}</span>
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name={student._id}
                    defaultChecked={student.graduated}
                    disabled={student.graduated}
                    ref={(input) => {
                      this.checkbox = input
                    }}
                  />
                  <label className="form-check-label" >Graduated</label>
                </li>
              )
            }
            )
          }
        </ul>

      </div>
    );
  }
}
export default App;