export const MARK_LIST_CONTRACT = "0x72Bb3b1a63198dE1D7F176a143e0Ecb90b2C0165";
export const MARK_LIST_ABI = [
    {
      "inputs": [],
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "name": "marks",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "cid",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "code",
          "type": "string"
        },
        {
          "internalType": "enum MarksList.Grades",
          "name": "grade",
          "type": "uint8"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_cid",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "_code",
          "type": "string"
        },
        {
          "internalType": "enum MarksList.Grades",
          "name": "_grade",
          "type": "uint8"
        }
      ],
      "name": "addMarks",
      "outputs": [
        {
          "components": [
            {
              "internalType": "uint256",
              "name": "cid",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "code",
              "type": "string"
            },
            {
              "internalType": "enum MarksList.Grades",
              "name": "grade",
              "type": "uint8"
            }
          ],
          "internalType": "struct MarksList.Marks",
          "name": "",
          "type": "tuple"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_cid",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "_code",
          "type": "string"
        }
      ],
      "name": "findMarks",
      "outputs": [
        {
          "components": [
            {
              "internalType": "uint256",
              "name": "cid",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "code",
              "type": "string"
            },
            {
              "internalType": "enum MarksList.Grades",
              "name": "grade",
              "type": "uint8"
            }
          ],
          "internalType": "struct MarksList.Marks",
          "name": "",
          "type": "tuple"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    }
  ]