export const STUDENT_LIST_CONTRACT = "0xF1CF183523eF3DEFDe52872b2b6242322F4C4a51"

export const STUDENT_LIST_ABI = [
        {
          "inputs": [],
          "stateMutability": "nonpayable",
          "type": "constructor"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "_id",
              "type": "uint256"
            },
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "_cid",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "string",
              "name": "name",
              "type": "string"
            },
            {
              "indexed": false,
              "internalType": "bool",
              "name": "graduated",
              "type": "bool"
            }
          ],
          "name": "createStudentEvent",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "_id",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "string",
              "name": "code",
              "type": "string"
            },
            {
              "indexed": false,
              "internalType": "string",
              "name": "subject",
              "type": "string"
            },
            {
              "indexed": false,
              "internalType": "bool",
              "name": "retired",
              "type": "bool"
            }
          ],
          "name": "createSubjectEvent",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "cid",
              "type": "uint256"
            }
          ],
          "name": "markGraduatedEvent",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "_id",
              "type": "uint256"
            }
          ],
          "name": "markRetiredEvent",
          "type": "event"
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "",
              "type": "string"
            }
          ],
          "name": "marks",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "cid",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "code",
              "type": "string"
            },
            {
              "internalType": "enum StudentList.Grades",
              "name": "grade",
              "type": "uint8"
            }
          ],
          "stateMutability": "view",
          "type": "function",
          "constant": true
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "name": "students",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "_id",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "cid",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "name",
              "type": "string"
            },
            {
              "internalType": "bool",
              "name": "graduated",
              "type": "bool"
            }
          ],
          "stateMutability": "view",
          "type": "function",
          "constant": true
        },
        {
          "inputs": [],
          "name": "studentsCount",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "stateMutability": "view",
          "type": "function",
          "constant": true
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "name": "subjects",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "_id",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "code",
              "type": "string"
            },
            {
              "internalType": "string",
              "name": "subject",
              "type": "string"
            },
            {
              "internalType": "bool",
              "name": "retired",
              "type": "bool"
            }
          ],
          "stateMutability": "view",
          "type": "function",
          "constant": true
        },
        {
          "inputs": [],
          "name": "subjectsCount",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "stateMutability": "view",
          "type": "function",
          "constant": true
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_studentCid",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "_name",
              "type": "string"
            }
          ],
          "name": "createStudent",
          "outputs": [
            {
              "components": [
                {
                  "internalType": "uint256",
                  "name": "_id",
                  "type": "uint256"
                },
                {
                  "internalType": "uint256",
                  "name": "cid",
                  "type": "uint256"
                },
                {
                  "internalType": "string",
                  "name": "name",
                  "type": "string"
                },
                {
                  "internalType": "bool",
                  "name": "graduated",
                  "type": "bool"
                }
              ],
              "internalType": "struct StudentList.Student",
              "name": "",
              "type": "tuple"
            }
          ],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [
            {
              "internalType": "string",
              "name": "_code",
              "type": "string"
            },
            {
              "internalType": "string",
              "name": "_subject",
              "type": "string"
            }
          ],
          "name": "createSubject",
          "outputs": [
            {
              "components": [
                {
                  "internalType": "uint256",
                  "name": "_id",
                  "type": "uint256"
                },
                {
                  "internalType": "string",
                  "name": "code",
                  "type": "string"
                },
                {
                  "internalType": "string",
                  "name": "subject",
                  "type": "string"
                },
                {
                  "internalType": "bool",
                  "name": "retired",
                  "type": "bool"
                }
              ],
              "internalType": "struct StudentList.Subject",
              "name": "",
              "type": "tuple"
            }
          ],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_id",
              "type": "uint256"
            }
          ],
          "name": "markRetired",
          "outputs": [
            {
              "components": [
                {
                  "internalType": "uint256",
                  "name": "_id",
                  "type": "uint256"
                },
                {
                  "internalType": "string",
                  "name": "code",
                  "type": "string"
                },
                {
                  "internalType": "string",
                  "name": "subject",
                  "type": "string"
                },
                {
                  "internalType": "bool",
                  "name": "retired",
                  "type": "bool"
                }
              ],
              "internalType": "struct StudentList.Subject",
              "name": "",
              "type": "tuple"
            }
          ],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_id",
              "type": "uint256"
            }
          ],
          "name": "markGraduated",
          "outputs": [
            {
              "components": [
                {
                  "internalType": "uint256",
                  "name": "_id",
                  "type": "uint256"
                },
                {
                  "internalType": "uint256",
                  "name": "cid",
                  "type": "uint256"
                },
                {
                  "internalType": "string",
                  "name": "name",
                  "type": "string"
                },
                {
                  "internalType": "bool",
                  "name": "graduated",
                  "type": "bool"
                }
              ],
              "internalType": "struct StudentList.Student",
              "name": "",
              "type": "tuple"
            }
          ],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_id",
              "type": "uint256"
            }
          ],
          "name": "findStudent",
          "outputs": [
            {
              "components": [
                {
                  "internalType": "uint256",
                  "name": "_id",
                  "type": "uint256"
                },
                {
                  "internalType": "uint256",
                  "name": "cid",
                  "type": "uint256"
                },
                {
                  "internalType": "string",
                  "name": "name",
                  "type": "string"
                },
                {
                  "internalType": "bool",
                  "name": "graduated",
                  "type": "bool"
                }
              ],
              "internalType": "struct StudentList.Student",
              "name": "",
              "type": "tuple"
            }
          ],
          "stateMutability": "view",
          "type": "function",
          "constant": true
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_id",
              "type": "uint256"
            }
          ],
          "name": "findSubject",
          "outputs": [
            {
              "components": [
                {
                  "internalType": "uint256",
                  "name": "_id",
                  "type": "uint256"
                },
                {
                  "internalType": "string",
                  "name": "code",
                  "type": "string"
                },
                {
                  "internalType": "string",
                  "name": "subject",
                  "type": "string"
                },
                {
                  "internalType": "bool",
                  "name": "retired",
                  "type": "bool"
                }
              ],
              "internalType": "struct StudentList.Subject",
              "name": "",
              "type": "tuple"
            }
          ],
          "stateMutability": "view",
          "type": "function",
          "constant": true
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_cid",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "_code",
              "type": "string"
            },
            {
              "internalType": "enum StudentList.Grades",
              "name": "_grade",
              "type": "uint8"
            }
          ],
          "name": "addMarks",
          "outputs": [
            {
              "components": [
                {
                  "internalType": "uint256",
                  "name": "cid",
                  "type": "uint256"
                },
                {
                  "internalType": "string",
                  "name": "code",
                  "type": "string"
                },
                {
                  "internalType": "enum StudentList.Grades",
                  "name": "grade",
                  "type": "uint8"
                }
              ],
              "internalType": "struct StudentList.Marks",
              "name": "",
              "type": "tuple"
            }
          ],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_cid",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "_code",
              "type": "string"
            }
          ],
          "name": "findMarks",
          "outputs": [
            {
              "components": [
                {
                  "internalType": "uint256",
                  "name": "cid",
                  "type": "uint256"
                },
                {
                  "internalType": "string",
                  "name": "code",
                  "type": "string"
                },
                {
                  "internalType": "enum StudentList.Grades",
                  "name": "grade",
                  "type": "uint8"
                }
              ],
              "internalType": "struct StudentList.Marks",
              "name": "",
              "type": "tuple"
            }
          ],
          "stateMutability": "view",
          "type": "function",
          "constant": true
        }
      ]