//Import the StudentList smart contract
const SubjectList = artifacts.require('SubjectList')

//Use the contract  to write all tests
//variable: account => all accounts in blockchain
contract('SubjectList', (accounts) => {
    //Make sure contract is deployed and before
    //we retrieve the studentlist object for testing
    beforeEach(async () => {
        this.subjectList = await SubjectList.deployed()
    })

    //Testing the deployed student contract
    it('deploys successfully', async () => {
        //Get the address which the student object is stored
        const address = await this.subjectList.address
        //Test for valid address
        isValidAddress(address)
    })

    it('creates subject successfully', async () => {
        const txn = await this.subjectList.createSubject('com212', 'computer science');
        isValidAddress(txn.tx);
        isValidAddress(txn.receipt.blockHash);
        const count = await this.subjectList.subjectsCount();
        assert.equal(count, 2);
        const subject = await this.subjectList.subjects(2);
        assert.equal(subject.code, 'com212');
    })

    it('finds subject successfully', async () => {
        const subject = await this.subjectList.findSubject(1);
        assert.equal(subject.code, 'com110');
    })

    it('marks retired successfully', async () => {
        await this.subjectList.markRetired(1);
        const subject = await this.subjectList.findSubject(1);
        assert.equal(subject.retired, true);
    })
})
//This function check if the address is valid
function isValidAddress(address) {
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}
