//Import the StudentList smart contract
const StudentList = artifacts.require('StudentList')

//Use the contract  to write all tests
//variable: account => all accounts in blockchain
contract('StudentList', (accounts) => {
    //Make sure contract is deployed and before
    //we retrieve the studentlist object for testing
    beforeEach(async () => {
        this.studentList = await StudentList.deployed()
    })

    //Testing the deployed student contract
    it('deploys successfully', async () => {
        //Get the address which the student object is stored
        const address = await this.studentList.address
        //Test for valid address
        isValidAddress(address)
    })

    it('test adding students', async () => {
        return StudentList.deployed().then((instance) => {
            studentlistInstance = instance;
            studentCid = 1;
            return studentlistInstance.createStudent(studentCid, "Pema Wango");
        }).then((transaction) => {
            isValidAddress(transaction.tx)
            isValidAddress(transaction.receipt.blockHash);
            return studentlistInstance.studentsCount()
        }).then((count) => {
            assert.equal(count, 2)
            return studentlistInstance.students(2);
        }).then((student) => {
            assert.equal(student.cid, studentCid)
        })
    })

    it('test finding students', async () => {
        return StudentList.deployed().then(async (instance) => {
            s = instance;
            studentCid = 2;
            return s.createStudent(studentCid++, "Ronnie Peh").then(async (tx) => {
                return s.createStudent(studentCid++, "Chanon Kulchol").then(async (tx) => {
                    return s.createStudent(studentCid++, "Sonam Wangmo").then(async (tx) => {
                        return s.createStudent(studentCid++, "Jigme Wangmo").then(async (tx) => {
                            return s.createStudent(studentCid++, "Samal Rai").then(async (tx) => {
                                return s.createStudent(studentCid++, "Tshering Tshering").then(async (tx) => {
                                    return s.studentsCount().then(async (count) => {
                                        assert.equal(count, 8)

                                        return s.findStudent(5).then(async (student) => {
                                            assert.equal(student.name, "Sonam Wangmo")
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
    it('test mark graduate students', async () => {
        return StudentList.deployed().then(async (instance) => {
            s = instance;
            const count = await s.studentsCount();
            console.log(count)
            return s.findStudent(1).then(async (ostudent) => {
                assert.equal(ostudent.name, "Rinchen Zangmo")
                assert.equal(ostudent.graduated, false)
                return s.markGraduated(1).then(async (transaction) => {
                    return s.findStudent(1).then(async (nstudent) => {
                        assert.equal(nstudent.name, "Rinchen Zangmo")
                        assert.equal(nstudent.graduated, true)
                        return
                    })
                })
            })
        })
    })


})
//This function check if the address is valid
function isValidAddress(address) {
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}
