//Import the StudentList smart contract
const MarkList = artifacts.require('MarksList')

//Use the contract  to write all tests
//variable: account => all accounts in blockchain
contract('MarksList', (accounts) => {
    //Make sure contract is deployed and before
    //we retrieve the studentlist object for testing
    beforeEach(async () => {
        this.markList = await MarkList.deployed()
    })

    //Testing the deployed student contract
    it('deploys successfully', async () => {
        //Get the address which the student object is stored
        const address = await this.markList.address
        //Test for valid address
        isValidAddress(address)
    })

    it('adds mark successfully', async () => {
        const txn = await this.markList.addMarks(10001, 'com110', 0);
        isValidAddress(txn.tx);
        isValidAddress(txn.receipt.blockHash);
        const marks = await this.markList.marks(10001, 'com110');
        assert.equal(marks.code, 'com110');
        assert.equal(marks.cid, 10001);
        assert.equal(marks.grade, 0);
    })

    it('finds mark successfully', async () => {
        const marks = await this.markList.findMarks(10001, 'com110');        
        assert.equal(marks.code, 'com110');
        assert.equal(marks.cid, 10001);
        assert.equal(marks.grade, 0);
    })
})

//This function check if the address is valid
function isValidAddress(address) {
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}
