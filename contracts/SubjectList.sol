// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
contract SubjectList {
 
    uint public subjectsCount = 0;
    struct Subject {
        uint _id;
        string code;
        string subject;
        bool retired;
    }

    mapping(uint => Subject) public subjects;

    event createSubjectEvent (uint _id, string code, string subject, bool retired );
    event markRetiredEvent (uint _id);


    constructor() {
        createSubject("com110", "computer science");
    }

    function createSubject(string memory _code, string memory _subject ) public returns (Subject memory) {
        subjectsCount++;
        subjects[subjectsCount] = Subject(subjectsCount, _code, _subject, false);
        emit createSubjectEvent(subjectsCount, _code, _subject, false);
        return subjects[subjectsCount];
    }

    function markRetired(uint _id) public returns (Subject memory) {
        subjects[_id].retired = true;
        emit markRetiredEvent(_id);
        return subjects[_id];
    }

    function findSubject(uint _id) public view returns (Subject memory) {
        return subjects[_id];
    }

}
