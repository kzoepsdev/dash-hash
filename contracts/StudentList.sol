// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
contract StudentList {
 
    enum Grades { A, B, C, D, FAIL }

    uint public studentsCount = 0;
    uint public subjectsCount = 0;
    struct Student {
        uint _id;
        uint cid;
        string name;
        bool graduated;
    }
    struct Subject {
        uint _id;
        string code;
        string subject;
        bool retired;
    }
    struct Marks {
        uint cid;
        string code;
        Grades grade;
    }

    mapping(uint => Subject) public subjects;
    mapping(uint => Student) public students;
    mapping(uint => mapping(string => Marks)) public marks;

    event createStudentEvent (
        uint _id,
        uint indexed _cid,
        string name,
        bool graduated
    );
    event createSubjectEvent (uint _id, string code, string subject, bool retired );
    event markGraduatedEvent (uint indexed cid);
    event markRetiredEvent (uint _id);


    constructor() {
        createStudent(10001, "Rinchen Zangmo");
        createSubject("com110", "computer science");
    }

    function createStudent(uint _studentCid, string memory _name) public returns (Student memory){
        studentsCount++;
        students[studentsCount] = Student(studentsCount,_studentCid, _name, false);
        emit createStudentEvent(studentsCount, _studentCid, _name, false);
        return students[studentsCount];
    }

    function createSubject(string memory _code, string memory _subject ) public returns (Subject memory) {
        subjectsCount++;
        subjects[subjectsCount] = Subject(subjectsCount, _code, _subject, false);
        emit createSubjectEvent(subjectsCount, _code, _subject, false);
        return subjects[subjectsCount];
    }

    function markRetired(uint _id) public returns (Subject memory) {
        subjects[_id].retired = true;
        emit markRetiredEvent(_id);
        return subjects[_id];
    }

    function markGraduated(uint _id) public returns (Student memory) {
        students[_id].graduated = true;
        emit markGraduatedEvent(_id);
        return students[_id];
    }

    function findStudent(uint _id) public view returns (Student memory){
        return students[_id];
    }

    function findSubject(uint _id) public view returns (Subject memory) {
        return subjects[_id];
    }

    function addMarks(uint _cid, string memory _code, Grades _grade) public returns (Marks memory) {
        // Marks memory tempMarks = Marks(_cid, _code, _grade);
        marks[_cid][_code] = Marks(_cid, _code, _grade);
        return marks[_cid][_code];
    }

    function findMarks(uint _cid, string memory _code) public view returns (Marks memory) {
        return marks[_cid][_code];
    }
}
